from hashlib import sha256
from time import time

class Transaction:
    def __init__(self, from_address, to_address, amount):
        self.from_address = from_address
        self.to_address = to_address
        self.amount = amount

class Block:
    def __init__(self, time_stamp, transactions, previous_hash):
        self.time_stamp = time_stamp
        self.transactions = transactions
        self.previous_hash = previous_hash
        self.nonce = 0
        self.hash = self.calculate_hash()

    def calculate_hash(self):
        data = (str(self.time_stamp) + ''.join(str(self.transactions)) +
                self.previous_hash + str(self.nonce))
        hashed_data = sha256(data.encode('utf-8')).hexdigest()
        return hashed_data
        
    def mine_block(self, difficulty):
        zeros = '0' * difficulty
        
        start_time = time()
        
        while self.hash[0:difficulty] != zeros:
            self.nonce += 1
            self.hash = self.calculate_hash()
            
        elapsed_time = time() - start_time
        
        print('• Block mined:', self.hash)
        print('• Nonce:', self.nonce)
        print('• Elapsed Time:', elapsed_time, 'milliseconds')
        
class Blockchain:
    def __init__(self):
        self.difficulty = 4
        self.mining_reward = 100
        self.chain = [self.create_genesis_block(),]
        self.pending_transactions = []
        
    def create_genesis_block(self):
        return Block('01.01.2020', [], '0')
    
    def get_lastest_block(self):
        return self.chain[-1]
    
    def mine_pending_transactions(self, miner_address):
        block = Block(time(), self.pending_transactions, self.get_lastest_block().hash)
        
        block.mine_block(self.difficulty)
        print('Block successfully mined!')
        self.chain.append(block)
        self.pending_transactions = [Transaction(None, miner_address, self.mining_reward)]
        
    def create_transaction(self, trans):
        self.pending_transactions.append(trans)
    
    def get_balance_of_address(self, address):
        balance = 0
        for block in self.chain:
            for trnc in block.transactions:
                
                if trnc.from_address == address:
                    balance -= trnc.amount
                if trnc.to_address == address:
                    balance += trnc.amount
        return balance
    
    def is_chain_valid(self):
        for i in range(1, len(self.chain)):
            current_block = self.chain[i]
            previous_block = self.chain[i - 1]
            if current_block.hash != previous_block.hash:
                return False
            return True


pouyanCoin = Blockchain()
pouyanCoin.create_transaction(Transaction('address1', 'address2', 100))
pouyanCoin.create_transaction(Transaction('address2', 'address3', 50))

print("\nStarting the Miner...")
pouyanCoin.mine_pending_transactions('pouyan-address')
print("\n")
print("Pouyan's balance is", pouyanCoin.get_balance_of_address('pouyan-address'))
print("A1's balance is", pouyanCoin.get_balance_of_address('address1'))
print("A2's balance is", pouyanCoin.get_balance_of_address('address2'))
print("A3's balance is", pouyanCoin.get_balance_of_address('address3'))

print("\nStarting the Miner...")
pouyanCoin.mine_pending_transactions('pouyan-address')
print("\n")
print("Pouyan's balance is", pouyanCoin.get_balance_of_address('pouyan-address'))
print("A1's balance is", pouyanCoin.get_balance_of_address('address1'))
print("A2's balance is", pouyanCoin.get_balance_of_address('address2'))
print("A3's balance is", pouyanCoin.get_balance_of_address('address3'))