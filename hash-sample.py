from hashlib import sha256

sample_string = input('Enter a string: ')
sample_string_hash = sha256(sample_string.encode('utf-8')).hexdigest()
print(sample_string_hash)